function Tree(layout){
this.layout=layout||new ElasticRowLayout()
}

/*
Tree.prototype.init
Tree.prototype.populate
Tree.prototype.pathToString
Tree.prototype.collapse
Tree.prototype.expand
Tree.prototype.onmouseout
Tree.prototype.onmouseover
Tree.prototype.refresh
*/

if (!Array.prototype.forEach) {
  Array.prototype.forEach = function(fun /*, thisp*/)
  {
    var len = this.length;
    if (typeof fun != "function")
      throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this)
        fun.call(thisp, this[i], i, this);
    }
  };
}

Tree.prototype.init=function(el,nl){
this.hoverPath=[]
this.activePath=[]
this.layout.init(el,nl,this)
}
Tree.prototype.setLayout=function(layout){
}
Tree.prototype.populate=function(depth){
while(depth--){
}
}
Tree.prototype.pathToString=function(nl){
var labels=[]
nl.forEach(function(n){labels.push(n.getLabel())})
return labels.join('/')
}
Tree.prototype.collapse=function(){
//log(this.pathToString(this.activePath))
}
Tree.prototype.expand=function(){
}
Tree.prototype.onmouseout=function(){
}
Tree.prototype.onmouseover=function(){
}
Tree.prototype.refresh=function(){
}
Tree.prototype.clearHoverPath=function(){//clear the hover path and redisplay the active path
this.layout.clearHoverPath(this.hoverPath)
}

log=window['console']?console.log:function(){}

Tree.util={
populate:function(n){
if(!n.isLeaf && !n.isPopulated){
n.children=n.getChildren()
n.children.forEach(function(c){c.nParent=n})
n.isPopulated=true
}
},
truncatePath:function(path,f,depth){//truncate a node list |path| to |depth|, removing |className| from any subsequent nodes
while(path.length>depth){
f(path.pop())
}
},
depth:function(n){//find the depth in the tree of a node, i.e. the number of ancestors to a node
var ret=0
while(n && n.nParent){n=n.nParent;ret++}
return ret
},
addClass:function(nd,cN){if(nd.className.search('(^| )'+cN+'( |$)')==-1) nd.className=nd.className.length?(nd.className+' '+cN):cN},
remClass:function(nd,cN){nd.className=nd.className.replace(new RegExp('(^| )'+cN+'( |$)'),'')},
isDescendant:function(e1,e2){//true if e2 is a descendant of, or equal to e1
do{if(e2===e1) return true} while (e2=e2.parentNode)
return false
},
}
