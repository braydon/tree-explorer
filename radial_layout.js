function RadialLayout(options){
this.options={
DIST:100,
}
for(opt in options) this.options[opt]=options[opt]
}

RadialLayout.prototype={
init:function(el,nl,tree){
var ths=this
el.onclick=function(e){e=e||event
ths.toggleMenu(el,nl,tree,e)
}
el.onmouseout=function(e){e=e||event
if(!Tree.util.isDescendant(el,e.relatedTarget)) tree.onmouseout()
}
},
clearHoverPath:function(hoverPath){
},
fillRow:function(el,nl,tree){
},
placeLabels:function(e,nl,a,o){
var i=0,l=nl.length,wrap,span,ret=[],DIST=this.options['DIST']
for (;i<l;i++){
span=document.createElement('span')
wrap=document.createElement('wrap')
span.appendChild(document.createTextNode(nl[i]===null?"×":nl[i].getLabel()))
wrap.appendChild(document.createTextNode(nl[i]===null?"×":nl[i].getLabel()))
span.className='cmd'
wrap.className='cmd-wrap'
ret.push(span)
var theta=a[2*i+1]
wrap.style.top=(o[0]-Math.cos(theta)*DIST)+'px'
//wrap.style.left=(o[1]+Math.sin(theta)*DIST)+'px'
//if(theta>Math.PI) wrap.style.left=(o[1]+Math.sin(theta)*DIST)+'px'
//else              wrap.style.left =(o[1]+Math.sin(theta)*DIST)+'px'
//wrap.style.left=(o[1]+Math.sin(theta)*DIST)+'px'
if(theta>Math.PI) wrap.style.right=(o[1]-Math.sin(theta)*DIST)+'px'
else              wrap.style.left =(o[1]+Math.sin(theta)*DIST)+'px'
span.className='cmd right'
//if(theta>Math.PI) span.className='cmd left'
//else span.className='cmd right'
e.appendChild(wrap).appendChild(span)
}return ret},
mmove:function(nl,a,o,l,p,tree){//menu::array, angles::array, offset::array(2), labels::array, parent::HTMLDivElement
var DIST=this.options.DIST,t=this
return function(e){
var x=e.pageX-o[0],y=e.pageY-o[1],i,len,dist=Math.sqrt(x*x+y*y)
for(i=0,len=l.length;i<len;i++)l[i].style.backgroundColor='#bbb'
i=t.pointedAt(a,x,y)
if(i!=null){
//anim(function(n){
// l[i].style.backgroundColor=rgbinter('bbb','#00f',n*n)
// },[],400)
l[i].style.backgroundColor=t.rgbinter('bbb','00f',dist/DIST)
if(dist>=DIST*0.8){
 if(nl[i]===null){
  p.parentNode.removeChild(p)
  if(p.parentMenu) t.activateMenu(p.parentMenu)
  tree.hoverPath.pop()
 }else{
 if(!nl[i].isLeaf){l[i].style.backgroundColor='#fff';l[i].style.color='#00f';Tree.util.populate(nl[i]);t.deactivateMenu(p);t.addLevel(tree,nl[i].children,e,p,a[i*2+1],DIST)}
 else if(tree.hoverPath[tree.hoverPath.length-1]!==nl[i]){tree.hoverPath.push(nl[i])}
}}}}},
toggleMenu:function(el,nl,tree,e){
this.addLevel(tree,nl,e)
},
addLevel:function(tree,nl,e,parent,theta,dist){
var div=document.createElement('div'),offset=[e.pageX,e.pageY],coords
if(parent&&theta&&dist)coords=this.chooseCoordsFromP(parent,theta,dist)
else coords=this.chooseCoordinates(e)
offset=[coords[0]+coords[2]/2,coords[1]+coords[3]/2]
this.setDimensions(div,coords)
div.className='menu'
div.parentMenu=parent
var nl2=[null].concat(nl),
angles=this.chooseAngles(nl2,theta),
//log(angles.join(', '))
labels=this.placeLabels(div,nl2,angles,[150,150])
document.body.appendChild(div)
div.onmousemove=this.mmove(nl2,angles,offset,labels,div,tree)
div.onmousedown=this.mclick(nl2,angles,offset,labels,div,tree)
},
chooseCoordsFromP:function(p,t,d){
var wx=300,wy=300,x=p.offsetLeft+(Math.sin(t)*d),y=p.offsetTop-(Math.cos(t)*d)
return [x>0?x:0,y>0?y:0,wx,wy]
},
chooseCoordinates:function(e){
var wx=300,wy=300,x=e.pageX-wx/2,y=e.pageY-wy/2
return [x,y,wx,wy]
},
setDimensions:function(e,a){with (e.style){
position="absolute"
left=a[0]+"px"
top=a[1]+"px"
width=a[2]+"px"
height=a[3]+"px"
}},
chooseAngles:function(nl,towards){
var n=nl.length*2,theta=Math.PI*2/n,r=[],i=0,offset=(towards==undefined)?0:towards-Math.PI-theta
for (;i<n;i++) r.push(this.normRad(i*theta+offset))
return r
},
normRad:function(t){var a=2*Math.PI //normalize to positive radians 0..2*PI
while (t<0) t+=a
while (t>a) t-=a
return t
},
pointedAt:function(a,x,y){
var sq=400
if(x*x+y*y<sq) return null
var theta=Math.atan2(x,-y)
if (theta<0) theta+=2*Math.PI
return this.inDirection(a,theta)
},
//return lowest x such that theta is between a[2x] and a[2(x+1)]
inDirection:function(a,theta){
for (var i=0,l=a.length;i<l;i+=2)if(this.between(a[i],a[i+2],theta))return i/2
return l/2-1
},
between:function(a,b,t){
if(b>a)return b>t && t>a
else return t>a || t<b
},
clamp:function(x,a,b){var c; if(a>b){c=b;b=a;a=c} if(x>b)return b; if(x<a)return a; return x},
rgbinter:function(from,to,by){//interpolate between two RGB values expressed as strings
by=this.clamp(by,0,1)
from=this.rgbStringToTriple(from)
to=this.rgbStringToTriple(to)
return this.rgbTripleToString({r:this.inter(from.r,to.r,by),g:this.inter(from.g,to.g,by),b:this.inter(from.b,to.b,by)})
},
rgbStringToTriple:function(s){
var m=/#?([0-9a-f]{1,2})([0-9a-f]{1,2})([0-9a-f]{1,2})/.exec(s.toLowerCase()),
f=function(s){return parseInt((s+s).substring(0,2),16)}
return{r:f(m[1]),g:f(m[2]),b:f(m[3])}
},
rgbTripleToString:function(t){
var f=function(n){return ('0'+(Math.round(n).toString(16))).substr(-2)}
return '#'+f(t.r)+f(t.g)+f(t.b)},
inter:function(a,b,x){return a+(b-a)*x},
mclick:function(nl,a,o,l,p,tree){
var t=this
return function(e){
var x=e.pageX-o[0],y=e.pageY-o[1],i
i=t.pointedAt(a,x,y)
if((i==null)||(nl[i]===null)){
p.parentNode.removeChild(p)
if(p.parentMenu) activateMenu(p.parentMenu)
}else{
if(typeof nl[i].onactivate =='function'){nl[i].onactivate()}tree.activePath=tree.hoverPath.slice(0);t.removeMenuTree(p)
}}},
deactivateMenu:function(e){
e.className='menu downlevel'
e.mmove_saved=e.onmousemove
e.mdown_saved=e.onmousedown
e.onmousemove=e.onmousedown=undefined
},
activateMenu:function(e){
e.className='menu'
e.onmousemove=e.mmove_saved
e.onmousedown=e.mdown_saved
e.mmove_saved=e.mdown_saved=undefined
},
removeMenuTree:function(e){
if (e.parentMenu) this.removeMenuTree(e.parentMenu)
e.parentNode.removeChild(e)
},
}

/*
onload=function(){
showMenu(menu.contents,e)
}
function Anim(f,a,d){this.f=f;this.args=a;this.duration=d||1000}
Anim.prototype.cancel=function(){this.finished=true}
runningAnimations={anims:[]}
function anim(f,args,dur){
var newAnim=new Anim(f,args,dur)
runningAnimations.anims.push(newAnim)
runAnims()
return newAnim
}
function runAnims(){
if(runningAnimations.intervalID!=undefined)return
runningAnimations.intervalID=setInterval(stepAnims,50)
}
function stepAnims(){
runningAnimations.anims.forEach(stepAnim)
if(!runningAnimations.anims.length){
 clearInterval(runningAnimations.intervalID)
 delete runningAnimations.intervalID
}}
function stepAnim(anim,i,arr){
var x
if (anim.finished) { arr.splice(i,1); return }
if (!anim.start) { anim.start = Number(new Date()); x=0 }
else { x = ((Number(new Date())) - anim.start) / anim.duration }
if (x>1) x=1
anim.f.apply(null,[x].concat(anim.args))
if (x==1) { arr.splice(i,1) }
}
*/
