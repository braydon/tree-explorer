function ElasticRowLayout(){ }

ElasticRowLayout.prototype.init=function(el,nl,tree){
var row=ElasticRowLayout.util.newRow()
this.fillRow(row,nl,tree)
el.appendChild(row)
ElasticRowLayout.util.setWidths(row)
el.onmouseout=function(e){e=e||event
if(!Tree.util.isDescendant(el,e.relatedTarget)) tree.onmouseout()
}
}
ElasticRowLayout.prototype.clearHoverPath=function(hoverPath){
hoverPath.forEach(function(n){Tree.util.remClass(n.panel,'hover');ElasticRowLayout.util.hideRow(n.childrenElement)})
}
ElasticRowLayout.prototype.fillRow=function(el,nl,tree){
var layout=this
if(!nl || !nl.length){el.appendChild(ElasticRowLayout.util.newPanel('empty'));return}
nl.forEach(function(n){
n.panel=ElasticRowLayout.util.newPanel(n.getLabel());n.panel.className=n.getClassNames()
el.appendChild(n.panel)
n.panel.onmouseover=function(e){
Tree.util.truncatePath(tree.hoverPath,function(n){Tree.util.remClass(n.panel,'hover');ElasticRowLayout.util.hideRow(n.childrenElement)},Tree.util.depth(n))
if(n.childrenElement){
ElasticRowLayout.util.unHideRow(n.childrenElement)
} else {
Tree.util.populate(n)
var row=ElasticRowLayout.util.newRow();layout.fillRow(row,n.children,tree);el.parentNode.appendChild(row);ElasticRowLayout.util.setWidths(row);n.childrenElement=row
}
tree.hoverPath.push(n)
Tree.util.addClass(n.panel,'hover')
}
n.panel.onclick=function(){if(typeof n.onactivate =='function'){n.onactivate()}tree.activePath=tree.hoverPath.slice(0)}
})
}

ElasticRowLayout.util={
newRow:function(){
var row=document.createElement('div');row.className='row'
return row
},
newPanel:function(s){
var p=document.createElement('span')
p.appendChild(document.createTextNode(s))
return p
},
setWidths:function(row){
var totalWidth=0,spans=Array.prototype.slice.call(row.childNodes,0)
spans.forEach(function(span){
totalWidth+=span.offsetWidth
})
if(totalWidth<row.clientWidth){
var extra=(row.clientWidth-totalWidth),
extraQuot=Math.floor(extra/(2*spans.length)),
extraRem=extra%(2*spans.length)
spans.forEach(function(span,i){
span.style.paddingLeft=extraQuot+'px'
span.style.paddingRight=(extraQuot+(i<extraRem?1:0))+'px'//XXX this doesn't necessarily add all the missing pixels
//log(i,span.style.paddingLeft,span.style.paddingRight)
})
}
},
hideRow:function(el){el.style.display='none'},
unHideRow:function(el){el.style.display='block'},
}
