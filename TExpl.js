log=window['console']?console.log:function(){}//XXX debugging

// add forEach to Array if it is missing, see https://developer.mozilla.org/En/Core_JavaScript_1.5_Reference:Objects:Array:forEach
if(!Array.prototype.forEach){
Array.prototype.forEach=function(f){
var len=this.length,thisp=arguments[1],i
for (i=0;i<len;i++){if(i in this){f.call(thisp, this[i], i, this)}}
}}

function TExpl(){
this.hoverPath=[]
this.activePath=[]
}

/*
TExpl.onhoverpathchange()
TExpl.show()
TExpl.hide()
TExpl.hoverPath
*/

TExpl.prototype.onmouseout=function(){}
/*
TExpl.prototype.init=function(el,nl){
this.layout.init(el,nl,this)
}
TExpl.prototype.show=function(){}
TExpl.prototype.setLayout=function(layout){
}
TExpl.prototype.populate=function(depth){
while(depth--){
}
}
TExpl.prototype.pathToString=function(nl){
var labels=[]
nl.forEach(function(n){labels.push(n.getLabel())})
return labels.join('/')
}
TExpl.prototype.collapse=function(){
//log(this.pathToString(this.activePath))
}
TExpl.prototype.expand=function(){
}
TExpl.prototype.onmouseout=function(){
}
TExpl.prototype.onmouseover=function(){
}
TExpl.prototype.refresh=function(){
}
TExpl.prototype.clearHoverPath=function(){//clear the hover path and redisplay the active path
this.layout.clearHoverPath(this.hoverPath)
}
*/

TExpl.util={
populate:function(n){
if(!n.isLeaf && !n.isPopulated){
n.children=n.getChildren()
n.children.forEach(function(c){c.nParent=n})
n.isPopulated=true
}
},
truncatePath:function(path,f,depth){//truncate a node list |path| to |depth|, calling |f| on each removed node
while(path.length>depth){
f(path.pop())
}
},
depth:function(n){//find the depth in the tree of a node, i.e. the number of ancestors to a node
var ret=0
while(n && n.nParent){n=n.nParent;ret++}
return ret
},
addClass:function(nd,cN){if(nd.className.search('(^| )'+cN+'( |$)')==-1) nd.className=nd.className.length?(nd.className+' '+cN):cN},
remClass:function(nd,cN){nd.className=nd.className.replace(new RegExp('(^| )'+cN+'( |$)'),'')},
isDescendant:function(e1,e2){//true if e2 is a descendant of, or equal to e1
do{if(e2===e1) return true} while (e2 && (e2=e2.parentNode))
return false
}
}

//XXX too many bytes per feature
TExpl.util.Anim=function(f,d){this.f=f;this.start=+new Date;this.duration=d||1000}
/*There should be Anim types for bounded animations which operate over a given duration, and for continuous animations*/
TExpl.util.Anim.prototype.cancel=function(){this.finished=true}
TExpl.util.Anim.running={anims:[]}
TExpl.util.anim=function(f,d){var a=new this.Anim(f,d);this.Anim.running.anims.push(a);this.Anim.runAnims();return a}
TExpl.util.Anim.runAnims=function(){if(this.running.intID){return} this.running.intID=setInterval(this.stepAnims,50)}
;(function(){var t=TExpl.util.Anim
TExpl.util.Anim.stepAnims=function(){if(!t.running.anims.length){t.clear()}else{t.running.anims.forEach(t.stepA)}}
})()
TExpl.util.Anim.clear=function(){clearInterval(this.running.intID);delete(this.running.intID)}
TExpl.util.Anim.stepA=function(a,i,as){if(a.finished){as=as.splice(i,1);return}a.f.call(a,((+new Date)-a.start))}

//XXX debugging
//;(function(){
//var st={velocity:0.1}
//var thing=TExpl.util.anim(function(){st.velocity+=.1;log(st);if(st.velocity>=3.14)thing.finished=true})
//})()

function ElasticRowTree(o,r){
this.root=r
var defaults=
{showEmpty:true
,emptyText:'empty'
,position:'top'
,grows:'down'
,layout:'horizontal' // the layout may be 'horizontal' or 'vertical'
,mouseStyle:'hover' // may be one of 'hover', 'click', 'click-once'
,padding:10 // the minimum padding required on panels, in pixels
,hsLabels:[' ← ',' → '] // the horizontal scroller labels
,hsMaxVel:0.9 // the maximum scroll velocity in pixels per millisecond
,hsA:0.000625 // auto-scroll ease-in parameter
,hsB:0.005 // auto-scroll edge slowdown
,domParent:null // the element in the document, if any, used as a parent for domNode
,domNode:null // the DOM element used as a parent for all nodes
}
for(i in defaults) this[i]=defaults[i]
for(i in o) this[i]=o[i]
//XXX should there be this.opts[i] or just this[i]?
//or some of each?
}

/* .util contains functions that only modify their arguments, and don't use |this|
 */
ElasticRowTree.util={
newRow:function(odd){
var row=document.createElement('div');row.className=odd?'row odd':'row'
return row
},
Span:function(t){
var p=document.createElement('span')
p.appendChild(document.createTextNode(t))
return p
},
newPanel:function(t){return this.Span(t)},
hideRow:function(el){if(el)el.style.display='none'},
unHideRow:function(el){if(el)el.style.display='block'}
}
for(i in TExpl.util){ElasticRowTree.util[i]=TExpl.util[i]} // !

ElasticRowTree.prototype=new TExpl()

;(function(){var
p=ElasticRowTree.prototype,
u=ElasticRowTree.util

p.show=function(){
if(this.domNode){this.domNode.style.display='block'}
else{var row=u.newRow(true),t=this
if(!this.domParent){this.domParent=document.body} //we deal with this case here since the object may be created before the DOM
if(!this.domNode){this.domNode=document.createElement('div')}
if(this.position=='top'){with(this.domNode.style){position='absolute';top='0'}}
if(this.position=='bottom'){with(this.domNode.style){position='absolute';bottom='0'}}
this.fillRow(row,[this.root])
this.domParent.appendChild(this.domNode)
this.domNode.appendChild(row)
this.setWidths(row)
this.domNode.onmouseout=function(e){e=e||event
if(!u.isDescendant(t.domNode,e.relatedTarget)) t.onmouseout()
}
}
}

p.hide=function(){if(!this.domNode)return
this.domNode.style.display='none'}

p.fillRow=function(el,nl){
var t=this
if(!nl || !nl.length){if(this.showEmpty){el.appendChild(u.newPanel(this.emptyText))}return}
nl.forEach(function(n){
 n.panel=u.newPanel(n.getLabel());n.panel.className=t.nodeClassName(n)
 el.appendChild(n.panel)
 function hover(e){
 u.truncatePath(t.hoverPath,function(n){u.remClass(n.panel,'hover');u.hideRow(n.childrenElement)},u.depth(n))
 if(n.childrenElement){u.unHideRow(n.childrenElement)}
 else{u.populate(n);if((n.children&&n.children.length)||t.showEmpty){var row=u.newRow(u.depth(n)%2);t.fillRow(row,n.children);t.domNode.appendChild(row);t.setWidths(row);n.childrenElement=row}}
 t.hoverPath.push(n)
 u.addClass(n.panel,'hover')
 }
 function click(){if(typeof n.onactivate =='function'){n.onactivate()}t.activePath=t.hoverPath.slice(0)}
// click-once implementation, commented out because it clobbers three document event handlers
// function h2(e){if(t.hoverMode){hover(e)}}
// function hOn(){t.hoverMode=true;document.onmousedown=document.onmousemove=function(){return false}}
// function hOff(){t.hoverMode=false}
 switch(t.mouseStyle){
 case 'hover':
  n.panel.onmouseover=hover
  n.panel.onclick=click
  break;
 case 'click':
  n.panel.onclick=function(e){hover(e);click()}
  break;
// case 'click-once':
//  n.panel.onmousedown=function(e){hover(e);hOn()}
//  n.panel.onmouseover=h2
//  document.onmouseup=function(e){hOff()}
//  break;
 default: throw t.mouseStyle+' unimplemented';break;
 }
})
}

p.nodeClassName=function(){return "node"}

p.setWidths=function(row){
var total=0,spans=[]
for(var i=0,l=row.childNodes.length;i<l;i++){spans.push(row.childNodes.item(i))}
spans.forEach(function(s){total+=s.offsetWidth})
if(total+(spans.length*2*this.padding) <= row.clientWidth){
var extra=(row.clientWidth-total),
extraQuot=Math.floor(extra/(2*spans.length)),
extraRem=extra%(2*spans.length)
spans.forEach(function(span,i){
span.style.paddingLeft=extraQuot+'px'
span.style.paddingRight=(extraQuot+(i<extraRem?1:0))+'px'//XXX this doesn't necessarily add all the missing pixels
//log(i,span.style.paddingLeft,span.style.paddingRight)
})
}
else{var s,w1,w2,rowW,sL,sR
/* There is not enough room in the widget for the panels.
 *
 * We create two wrapper divs, w1 and w2, and left and right scroller spans.
 * When we are done, the row has three children: the scroller spans on the
 * left and right, and the wrapper w1, which sits between the scrollers.
 * w1 has overflow:hidden and has one child, w2, which contains the panels,
 * is wide enough to display all the panels in one row, is cropped by w1,
 * and may be moved left or right by means of the scrollers.
 */
/* It's entirely possible that |total| is completely wrong, since one of the spans 
 * may have wrapped, in which case offsetWidth will be much too large.  To correct 
 * for this edge case we set the width of the row to the calculated total, and then 
 * take all the span widths again.
 * Before doing so we save the width of the row which we need later.
 */
rowW=row.clientWidth
row.style.width=total+(spans.length*2*(this.padding+2))+'px'
/* before recalculating the width we add the padding we want to the panels */
spans.forEach(function(sp){sp.style.paddingLeft=sp.style.paddingRight=this.padding+'px'},this)
total=0
spans.forEach(function(s){total+=s.offsetWidth})
w2=document.createElement('div')
w1=document.createElement('div')
while(row.firstChild){w2.appendChild(row.firstChild)}
sL=u.Span(this.hsLabels[0]);with(sL.style){position='absolute';left ='0'};row.appendChild(sL)
sR=u.Span(this.hsLabels[1]);with(sR.style){position='absolute';right='0'};row.appendChild(sR)
/* the auto-scrolling animation has the following goals:
 * - fast enough that the user doesn't feel they are waiting for it
 * - slow enough that the user doesn't miss things that are moving past
 * - starts slow so the user realizes what is happening
 * - slows down as it nears either end of the scrollable range
 *
 * We use the elapsed time since the start of the animation and the
 * distance from the end we are moving towards to calculate the current
 * velocity in pixels per millisecond.  We then use the velocity, the
 * elapsed time since the previous animation step, and the previous
 * position to calculate the current position.
 *
 * vel = min( MAXVEL, A * elapsed time, B * distance from edge)
 *
 * where A and B are constants
 *
 * This can be replaced by the more general:
 * vel = min( MAXVEL, f1(total_ms), f2(edge_dist)
 * where f1 and f2 are arbitrary easing functions
 */

var st={pos:0},anim,t=this
sR.onmouseover=function(){
 st.lim=total-w1.clientWidth;st.prevT=null
 anim=u.anim(function(ms){var
 tm=+new Date,v=Math.min(t.hsMaxVel,t.hsA*ms,t.hsB*(st.lim-st.pos))
 st.pos+=v*(tm-(st.prevT||anim.start))
 w2.style.left='-'+st.pos+'px'
 st.prevT=tm})}
sR.onmouseout=function(){anim.cancel()}//XXX ease out
sL.onmouseover=function(){
 st.lim=0;st.prevT=null
 anim=u.anim(function(ms){var
 tm=+new Date,v=Math.min(t.hsMaxVel,t.hsA*ms,t.hsB*st.pos)
 st.pos-=v*(tm-(st.prevT||anim.start))
 w2.style.left='-'+st.pos+'px'
 st.prevT=tm})}
sL.onmouseout=function(){anim.cancel()}


with(row.style){width=rowW+'px';position='relative'} //XXX handle resize events
with(w2.style){width=total+'px';position='relative';left='0'}
//row.parentNode.replaceChild(w2,row);w2.appendChild(w1);w1.appendChild(row)
row.appendChild(w1);w1.appendChild(w2)
with(w1.style){position='relative';width=rowW-sR.offsetWidth-sL.offsetWidth+'px';left=sL.offsetWidth+'px';overflow='hidden'}
}
}

})()

/*
ElasticRowTree.prototype.show=function(el,nl,tree){
}
}
ElasticRowLayout.prototype.clearHoverPath=function(hoverPath){
hoverPath.forEach(function(n){Tree.util.remClass(n.panel,'hover');u.hideRow(n.childrenElement)})
}
ElasticRowLayout.prototype.fillRow=function(el,nl,tree){
}
*/

